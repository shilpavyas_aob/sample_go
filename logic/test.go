package logic

import (
	"context"
	"fmt"
	"grpc_test/config"
	"grpc_test/database"
	test "grpc_test/pb"
	"log"
)

var repo database.MongoRepo

type CommonStruct struct {
}

func CreateConnection(cfg *config.Config) {
	response, error := database.CreateConnection(cfg)
	repo = response
	if error != nil {
		panic(fmt.Sprintf("Error while connecting to database"))
	}
	//defer repo.Close()
}

func (c CommonStruct) CreateProduct(d context.Context, products *test.Products) (*test.Success, error) {
	err := repo.CreateProducts(d, products)
	if err != nil {
		log.Fatalf("Error while creating products: %v", err)
	}
	return &test.Success{
		Code:    "200",
		Message: "Product successfully created",
	}, nil
}

func (c CommonStruct) GetProduct(d context.Context, t *test.EmptyObject) (*test.Products, error) {
	products, err := repo.GetProducts(d)
	if err != nil {
		log.Fatalf("Error while creating products: %v", err)
	}
	return &test.Products{
		Products: products,
	}, nil
}

type NewCommonService struct {
	CommonStruct
}

func GetService() NewCommonService {
	service := NewCommonService{
		CommonStruct: CommonStruct{},
	}
	return service
}
