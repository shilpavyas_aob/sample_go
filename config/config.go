package config

import "fmt"

type Config struct {
	Db, Usr, Pwd, MongoHost string
}

func LocalConfig() *Config {
	return &Config{
		Db:        "stem_cells",
		MongoHost: "localhost:27017",
		Usr:       "",
		Pwd:       "",
	}
}

func DevConfig() *Config {
	return &Config{
		Db:        "supplier_production",
		MongoHost: "13.232.153.76:27017",
		Usr:       "",
		Pwd:       "",
	}
}
func (c Config) String() string {
	return fmt.Sprintf(
		"MongoHost[%v], Db[%v], Usr[%v], Pwd[%v]",
		c.MongoHost, c.Db, c.Usr, c.Pwd)
}
