package random

type Customer struct {
}

type CustomerRepo interface {
	handleCustomer(cust Customer) error
}

type CustomerRepoImpl struct {
}

func (c CustomerRepoImpl) handleCustomer(cust Customer) error {
	return nil
}

type CustomerPublisher interface {
	handleCustomer(cust Customer) error
}

type CustomerPublisherImpl struct {
}

func (c CustomerPublisherImpl) handleCustomer(cust Customer) error {
	return nil
}

type Service struct {
	CustomerRepo
	CustomerPublisher
}

func something() {
	service := Service{
		CustomerRepo:      CustomerRepoImpl{},
		CustomerPublisher: CustomerPublisherImpl{},
	}

	service.CustomerPublisher.handleCustomer(Customer{})
}

type MyInt int

func (m MyInt) Increment() {

}
