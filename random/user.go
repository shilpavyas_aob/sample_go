package random

import (
	"context"
	test "grpc_test/pb"
)

type Random struct {
}

func (r Random) GetRandommUser(c context.Context, i *test.Customer) (*test.Error, error) {
	return &test.Error{
		Name: i.Name,
	}, nil
}
