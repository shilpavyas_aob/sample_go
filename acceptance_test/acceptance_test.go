package acceptance_test

import (
	"context"
	"fmt"
	test "grpc_test/pb"
	"testing"

	"google.golang.org/grpc"
)

func TestCanShowPerson(t *testing.T) {
	conn, client := createClient(t)
	defer conn.Close()
	response, e := client.ShowPerson(context.Background(), &test.WhoAreYouParams{First: 6.0})
	fail(t, e)
	var expected float32
	expected = 6.0
	if response.Result != expected {
		t.Errorf(fmt.Sprintf("Expected %v but got %v", expected, response.Result))
	}
}

func TestCanDonotShowPerson(t *testing.T) {
	conn, client := createClient(t)
	defer conn.Close()
	response, e := client.DonotShowPerson(context.Background(), &test.WhoAreYouParams{First: 6.0})
	fail(t, e)
	var expected float32
	expected = 6.0
	if response.Result != expected {
		t.Errorf(fmt.Sprintf("Expected %v but got %v", expected, response.Result))
	}
}
func createClient(t *testing.T) (*grpc.ClientConn, test.ServiceUserClient) {
	conn, err := grpc.Dial(":8080", grpc.WithInsecure())
	fail(t, err)
	//defer conn.Close()
	client := test.NewServiceUserClient(conn)
	return conn, client
}

func fail(t *testing.T, err error) {
	if err != nil {
		t.Errorf(err.Error())
	}
}
