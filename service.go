package main

import (
	"flag"
	"fmt"
	"grpc_test/config"
	"grpc_test/database"
	"grpc_test/logic"
	test "grpc_test/pb"
	"log"
	"net"

	"google.golang.org/grpc"
)

var env = flag.String("env", "", "Environment. One of {local,dev, test, prod}")
var repo database.MongoRepo

func main() {
	flag.Parse()
	if env == nil || *env == "" {
		panic(fmt.Sprintf("environment not specified"))
	}
	var cfg *config.Config
	switch *env {
	case "local":
		cfg = config.LocalConfig()
		break
	case "dev":
		cfg = config.DevConfig()
		break
	default:
		panic(fmt.Sprintf("environment not valid"))
		break
	}
	logic.CreateConnection(cfg)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8080))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	uiService := logic.GetService()
	test.RegisterServiceUserServer(grpcServer, uiService)
	log.Println("Starting server.")
	grpcServer.Serve(lis)
	log.Println("Server started")
}
