package database

import (
	"context"
	"fmt"
	"grpc_test/config"
	test "grpc_test/pb"
	"time"

	"github.com/prometheus/common/log"
	mgo "gopkg.in/mgo.v2"
)

type Repository interface {
	CreateProducts(ctx context.Context, products *test.Products) error
	GetProducts(ctx context.Context) ([]*test.Product, error)
	Close()
}
type MongoRepo struct {
	session *mgo.Session
}

const productCollection = "products"

var dbSettings config.Config

func (m *MongoRepo) getCollection(collection string) *mgo.Collection {
	return m.session.DB(dbSettings.Db).C(collection)
}

func (m *MongoRepo) CreateProducts(ctx context.Context, products *test.Products) error {
	bulk := m.getCollection(productCollection).Bulk()
	bulk.Unordered()
	for i := 0; i < len(products.Products); i++ {
		bulk.Insert(products.Products[i])
	}

	_, bulkErr := bulk.Run()
	if bulkErr != nil {
		return bulkErr
	}
	return nil
}

func (m *MongoRepo) GetProducts(ctx context.Context) ([]*test.Product, error) {
	var product []test.Product
	err := m.getCollection(productCollection).Find(nil).All(&product)
	if err != nil {
		fmt.Print("error while getting products")
	}
	productSample := []*test.Product{}
	for i := 0; i < len(product); i++ {
		productSample = append(productSample, &product[i])
	}
	return productSample, nil
}

func CreateConnection(config *config.Config) (MongoRepo, error) {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{config.MongoHost},
		Timeout:  60 * time.Second,
		Database: config.Db,
		Username: config.Usr,
		Password: config.Pwd,
	}

	sess, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		panic(fmt.Sprintf("%v", err))
	}
	log.Infoln("Successfully connected to Mongo.")
	return MongoRepo{session: sess}, nil
}

func (m MongoRepo) Close() {
	log.Infoln("Closing the mongo sessions.")
	m.session.Close()
	log.Infoln("Mongo Sessions closed.")
}
