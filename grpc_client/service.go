package main

import (
	"context"
	"encoding/json"
	test "grpc_test/pb"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"google.golang.org/grpc"
)

var conn *grpc.ClientConn
var c test.ServiceUserClient

func main() {

	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to start gRPC connection: %v", err)
	}
	defer conn.Close()
	c = test.NewServiceUserClient(conn)
	forever := make(chan bool)
	router := mux.NewRouter()
	go func() {
		router.HandleFunc("/products", CreateProducts).Methods("POST")
		router.HandleFunc("/products", GetProducts).Methods("GET")
		handler := cors.Default().Handler(router)
		log.Fatal(http.ListenAndServe(":3000", handler))

	}()

	<-forever
}

func CreateProducts(writer http.ResponseWriter, request *http.Request) {
	body, _ := ioutil.ReadAll(io.LimitReader(request.Body, 1048576))
	incomingRequest := &test.Products{}
	err := json.Unmarshal(body, incomingRequest)
	if err != nil {
		log.Fatalf("Error when Unmarshaling body: %s", err)
	}
	response, err := c.CreateProduct(context.Background(), incomingRequest)
	if err != nil {
		log.Fatalf("Error while creating products: %s", err)
	}
	json.NewEncoder(writer).Encode(response)
}

func GetProducts(writer http.ResponseWriter, request *http.Request) {
	response, err := c.GetProduct(context.Background(), &test.EmptyObject{})
	if err != nil {
		log.Fatalf("Error while creating products: %s", err)
	}
	json.NewEncoder(writer).Encode(response)
}
